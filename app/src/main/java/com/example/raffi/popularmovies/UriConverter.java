package com.example.raffi.popularmovies;

import android.arch.persistence.room.TypeConverter;
import android.net.Uri;

public class UriConverter {

	@TypeConverter
	public String toString(Uri uri) {
		return uri.toString();
	}

	@TypeConverter
	public Uri toUri(String str) {
		return Uri.parse(str);
	}

}
