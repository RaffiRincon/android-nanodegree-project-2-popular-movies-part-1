package com.example.raffi.popularmovies;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface FavoriteMovieDao {
	@Query("SELECT * FROM favorite_movie")
	LiveData<List<MoviePoster>> getAll();

	@Insert
	void insert(MoviePoster favoriteMovie);

	@Delete
	void delete(MoviePoster favoriteMovie);
}
