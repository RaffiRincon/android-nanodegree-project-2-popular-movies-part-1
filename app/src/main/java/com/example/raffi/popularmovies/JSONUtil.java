package com.example.raffi.popularmovies;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

class JSONUtil {

    private final static String MOVIEDB_SCHEME = "https";
    private final static String MOVIEDB_AUTHORITY = "image.tmdb.org";
    private final static String MOVIEDB_DB_PATH_IMAGE_STANDARD_RES = "t/p/w185";
    private final static String MOVIEDB_DB_PATH_IMAGE_HIGH_RES = "t/p/w400";

    public static List<MoviePoster> moviePosters(String jsonString, Context context) {
        ArrayList<MoviePoster> moviePosters = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(jsonString);

            Log.d(NetworkUtil.class.getName(), "JSON: " + json.toString());

            JSONArray movieJSONs = json.getJSONArray("results");

            if (movieJSONs == null) {
                return null;
            }

            for (int i = 0; i < movieJSONs.length(); i++) {
                JSONObject movieJSON = movieJSONs.getJSONObject(i);

                MoviePoster moviePoster = new MoviePoster();

                String posterPath = movieJSON.getString("poster_path");

				moviePoster.imageURI = new Uri.Builder()
						.scheme(MOVIEDB_SCHEME)
						.authority(MOVIEDB_AUTHORITY)
						.path(MOVIEDB_DB_PATH_IMAGE_STANDARD_RES + posterPath)
						.appendQueryParameter(NetworkUtil.MOVIE_DB_API_KEY_LABEL, NetworkUtil.MOVIE_DB_API_KEY)
						.build();

				moviePoster.highResImageUri = new Uri.Builder()
                        .scheme(MOVIEDB_SCHEME)
                        .authority(MOVIEDB_AUTHORITY)
                        .path(MOVIEDB_DB_PATH_IMAGE_HIGH_RES + posterPath)
                        .appendQueryParameter(NetworkUtil.MOVIE_DB_API_KEY_LABEL, NetworkUtil.MOVIE_DB_API_KEY)
                        .build();

                moviePoster.title = movieJSON.getString("title");
                moviePoster.id = movieJSON.getInt("id");

                moviePoster.plotSynopsis = movieJSON.getString("overview");
                moviePoster.averageRating = movieJSON.getDouble("vote_average");
                moviePoster.popularity = movieJSON.getDouble("popularity");

                String releaseDateString = movieJSON.getString("release_date");
                DateFormat dateFormatMatchingMovieDB = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

				try {
					Date releaseDate = dateFormatMatchingMovieDB.parse(releaseDateString);
					DateFormat dateFormatWithUserLocale = android.text.format.DateFormat.getDateFormat(context);
					releaseDateString = dateFormatWithUserLocale.format(releaseDate);
				} catch (ParseException parseException) {
					Log.e(JSONUtil.class.getName(), "Parse Error while parsing date from MovieDB JSON: " + parseException);
					parseException.printStackTrace();
				}

				moviePoster.releaseDateString = releaseDateString;

                moviePosters.add(moviePoster);
            }

            return moviePosters;
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }

        return null;
    }
}
