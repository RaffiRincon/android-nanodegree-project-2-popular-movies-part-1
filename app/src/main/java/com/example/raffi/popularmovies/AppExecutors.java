package com.example.raffi.popularmovies;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AppExecutors {

	private final Executor diskIOExecutor;
	private static final Object LOCK = new Object();
	private static AppExecutors instance;

	private AppExecutors(Executor diskIOExecutor) {
		this.diskIOExecutor = diskIOExecutor;
	}

	public static AppExecutors sharedInstance() {
		if (instance == null) {
			synchronized (LOCK) {
				instance = new AppExecutors(Executors.newSingleThreadExecutor());
			}
		}

		return instance;
	}

	public Executor getDiskIOExecutor() {
		return diskIOExecutor;
	}
}
