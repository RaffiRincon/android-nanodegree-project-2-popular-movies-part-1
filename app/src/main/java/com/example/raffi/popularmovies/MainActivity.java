package com.example.raffi.popularmovies;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

	private MoviePosterAdapter moviePosterAdapter;
	private MovieListMode movieListMode = MovieListMode.ORDER_BY_POPULARITY;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		RecyclerView moviePosterRecyclerView = findViewById(R.id.rv_grid);
		moviePosterAdapter = new MoviePosterAdapter(this, null);
		moviePosterRecyclerView.setAdapter(moviePosterAdapter);

		GridLayoutManager layoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
		moviePosterRecyclerView.setLayoutManager(layoutManager);

		new MovieDBTask().execute(MovieListMode.ORDER_BY_POPULARITY);

		setTitle(R.string.activity_main_title);

		configureViewModel();
	}

	enum MovieListMode {
		ORDER_BY_POPULARITY, ORDER_BY_TOP_RATED, SHOW_FAVORITES
	}

	class MovieDBTask extends AsyncTask<MovieListMode, Void, List<MoviePoster>> {

		@Override
		protected List<MoviePoster> doInBackground(MovieListMode... sortOrders) {
			String jsonString;

			MovieListMode sortOrder = sortOrders[0];

			try {
				URL url = null;

				switch (sortOrder) {
					case ORDER_BY_TOP_RATED:
						url = NetworkUtil.buildTopRatedMoviesURL();
						break;
					case ORDER_BY_POPULARITY:
						url = NetworkUtil.buildPopularMoviesURL();
				}

				jsonString = NetworkUtil.getResponse(url);

				return JSONUtil.moviePosters(jsonString, MainActivity.this);
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(List<MoviePoster> moviePosters) {
			if (moviePosters != null && moviePosters.size() > 0) {
				moviePosterAdapter.setMoviePosters(moviePosters);
			}
		}
	}

	private void configureViewModel() {
		MainViewModel viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
		viewModel.getFavoriteMovies().observe(this, new Observer<List<MoviePoster>>() {
			@Override
			public void onChanged(@Nullable List<MoviePoster> favoriteMovies) {
				moviePosterAdapter.setFavoriteMovies(favoriteMovies);
				if (movieListMode == MovieListMode.SHOW_FAVORITES) {
					moviePosterAdapter.showFavoriteMovies();
				}
			}
		});
	}

	private void showFavoritedMoviePosters() {
		moviePosterAdapter.showFavoriteMovies();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		item.setChecked(true);

		MovieListMode newMovieListMode = null;

		switch (item.getItemId()) {
			case R.id.main_activity_menu_item_popularity:
				newMovieListMode = MovieListMode.ORDER_BY_POPULARITY;
				break;
			case R.id.main_activity_menu_item_top_rated:
				newMovieListMode = MovieListMode.ORDER_BY_TOP_RATED;
				break;
			case R.id.main_activity_menu_item_show_favorites:
				newMovieListMode = MovieListMode.SHOW_FAVORITES;
				break;
			default:
				return super.onOptionsItemSelected(item);
		}

		if (movieListMode != newMovieListMode) {
			movieListMode = newMovieListMode;
			if (movieListMode == MovieListMode.SHOW_FAVORITES) {
				showFavoritedMoviePosters();
			} else {
				new MovieDBTask().execute(movieListMode);
			}
		}

		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.main_activity_menu, menu);

		return true;
	}
}
