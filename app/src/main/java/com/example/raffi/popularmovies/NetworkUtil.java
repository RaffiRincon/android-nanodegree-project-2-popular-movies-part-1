package com.example.raffi.popularmovies;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtil {

    public static final String MOVIE_DB_API_KEY = BuildConfig.ApiKey;
    public static final String MOVIE_DB_API_KEY_LABEL = "api_key";


    private static final String BASE_MOVIES_PATH = "http://api.themoviedb.org/3/movie/";

    private static final String MOVIES_BY_POPULARITY_PATH = BASE_MOVIES_PATH + "popular";
    private static final String MOVIES_BY_RATING_PATH = BASE_MOVIES_PATH + "top_rated";

    public static URL buildPopularMoviesURL() {
        Uri uri = new Uri.Builder()
                .encodedPath(MOVIES_BY_POPULARITY_PATH)
                .appendQueryParameter(MOVIE_DB_API_KEY_LABEL, MOVIE_DB_API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(uri.toString());

        } catch (MalformedURLException badURLException) {
            badURLException.printStackTrace();
        }

        return url;
    }

    public static URL buildTopRatedMoviesURL() {
        Uri uri = new Uri.Builder()
                .encodedPath(MOVIES_BY_RATING_PATH)
                .appendQueryParameter(MOVIE_DB_API_KEY_LABEL, MOVIE_DB_API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(uri.toString());

        } catch (MalformedURLException badURLException) {
            badURLException.printStackTrace();
        }

        return url;
    }

    public static String getResponse(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
