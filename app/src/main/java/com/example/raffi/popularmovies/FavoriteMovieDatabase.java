package com.example.raffi.popularmovies;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

@Database(entities = MoviePoster.class, version = 1, exportSchema = false)
@TypeConverters(UriConverter.class)
public abstract class FavoriteMovieDatabase extends RoomDatabase {

	public abstract FavoriteMovieDao getFavoriteMovieDao();

	private static final String DATABASE_NAME = "favorite_movies";
	private static FavoriteMovieDatabase sDatabase;
	private static final Object LOCK = new Object();

	public static FavoriteMovieDatabase getDatabase(Context context) {
		if (sDatabase == null) {
			synchronized (LOCK) {
				sDatabase = Room.databaseBuilder(context.getApplicationContext(),
						FavoriteMovieDatabase.class, DATABASE_NAME).build();
			}
		}
		return sDatabase;
	}
}
