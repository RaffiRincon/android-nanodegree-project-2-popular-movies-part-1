package com.example.raffi.popularmovies;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

	private LiveData<List<MoviePoster>> favoriteMovies;

	public MainViewModel(@NonNull Application application) {
		super(application);

		final FavoriteMovieDatabase database = FavoriteMovieDatabase
				.getDatabase(this.getApplication());
		favoriteMovies = database.getFavoriteMovieDao().getAll();
	}

	public LiveData<List<MoviePoster>> getFavoriteMovies() {
		return favoriteMovies;
	}
}
